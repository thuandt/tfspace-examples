describe "main" do
  before(:all) do
    reconfigure_logging # reconfigure Terraspace.logger to a file
    mod_path = File.expand_path("../..", __dir__) # the source of the module to test is 2 levels up
    # Build terraspace project to use as a test harness
    # Will be located at: /tmp/terraspace/test-harnesses/mybug-harness
    terraspace.build_test_harness(
      name: "mybug-harness",
      modules: {mybug: mod_path},
    )
    terraspace.up("mybug")
  end
  after(:all) do
    terraspace.down("mybug")
  end

  it "successful deploy" do
    pp terraspace.outputs
    output_value = terraspace.output("mybug", "myvar")
    expect(output_value).to include("this-is-a-bug")
  end
end
